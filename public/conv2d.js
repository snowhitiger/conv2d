let canvas;
let img;
let conv_img;
let random_weights = [];
let fixed_filter = [];
let random_filter = [];
let conv_filter;

let radio;

let x0, y0; // starting coordinate of the source image
let x1, y1; // starting coordinate of the convoluted image
let w, h; // width and height of the images
let total_conv_iter;
let conv_iter;

function windowResized() {
    resizeCanvas(windowWidth, windowHeight);
}

function preload() {
    img = loadImage('playboy.png');
}

function get_conv_iterations(img) {
    //console.log(img.width);
    //console.log(img.height);
    return img.width * img.height;
}

function setup() {
    createCanvas(windowWidth, windowHeight);
    background(255);

    x0 = 0.2 * height;
    y0 = 0.2 * height;
    w = Math.floor(0.4 * height);
    h = Math.floor(0.4 * height);
    x1 = 0.7 * width;
    y1 = 0.2 * height;

    // create convoluted image
    conv_img = createImage(w-2, h-2);
    conv_img.loadPixels();

    radio = createRadio();
    radio.option('Edge Detection');
    radio.option('Random');
    radio.style('width', '400px');
    radio.style("font-size", "20px");
    radio.position(0.4*width, 0.6*height);
    radio._getInputChildrenArray()[0].checked = true;
    textAlign(CENTER);

    total_conv_iter = get_conv_iterations(conv_img);
    //console.log(total_conv_iter);

    // set values for fixed and random convolution filter
	fixed_filter = [0, 1, 0, 1, -4, 1, 0, 1, 0];
    set_random_filter();
}

function set_random_filter(){
    for (let i=0; i<9; i++) {
        random_filter[i] = random(-0.8, 1.0);
    }
}

function truncate_color(c) {
    let res = c;
    if (c > 255) {
       res = 255;
    } else if (c < 0) {
       res = 0;
    }
    return Math.floor(res);
}

function draw() {
    // choose which filter to use
    let choice = radio.value();

    image(img, x0, y0, w, h);
    let pix = get(x0, y0, w, h);
    pix.loadPixels();
    //console.log(pix.pixels[0]);

    textSize(30);
    textAlign(CENTER, BOTTOM);
    text("Input", x0+w/2, 0.5*y0);
    text("Filter", 0.4*width + 0.1*width, 0.5*y0);
    text("Feature Map", x1+w/2, 0.5*y0);

    // grid shape
    shape = [3, 3];

    conv_filter = fixed_filter;
    if (choice) {
       if (choice == "Random") {
           conv_filter = random_filter;
       } 
    } 

	grids(0.4*width, 0.2*height, 0.2*width, 0.2*width, shape, conv_filter);

    let x, y;
    let group = 1024;
    conv_iter = (frameCount * group) % total_conv_iter;
    let it;
    for (let i=0; i<group; i++) {
        it = conv_iter + i;
        y = Math.floor(it / conv_img.width);
        x = Math.floor(it - y * conv_img.width);
        conv2d(conv_img, pix, conv_filter, x, y);

        if (it == total_conv_iter) {
            set_random_filter();
        }
    }

    conv_img.updatePixels();
    image(conv_img, x1, y1);

}

function conv2d(image, src, filter, x, y) {
    let red = 0.0;
    let green = 0.0;
    let blue = 0.0;
    let alpha = 0.0;

    for (let i=0; i<3; i++) 
      for (let j=0; j<3; j++) {
        let index = (x+i + (y+j) * src.width) * 4;
        let src_r = src.pixels[index];
        let src_g = src.pixels[index+1];
        let src_b = src.pixels[index+2];
        let src_a = src.pixels[index+3];
        
        red += filter[i+j*3] * src_r;
        green += filter[i+j*3] * src_g;
        blue += filter[i+j*3] * src_b;
        alpha += filter[i+j*3] * src_a;
    }

    red = truncate_color(red);
    green = truncate_color(green);
    blue = truncate_color(blue);
    //alpha = truncate_color(alpha);
    alpha = 100;

    //console.log(red);
    //console.log(green);
    //console.log(blue);
    //console.log(alpha);

    let idx = (x + y * image.width) * 4;
    image.pixels[idx] = red;
    image.pixels[idx + 1] = green;
    image.pixels[idx + 2] = blue;
    image.pixels[idx + 3] = alpha;
}


function writeColor(image, x, y, red, green, blue, alpha) {
  let index = (x + y * image.width) * 4;
  image.pixels[index] = red;
  image.pixels[index + 1] = green;
  image.pixels[index + 2] = blue;
  image.pixels[index + 3] = alpha;
}

function grids(posx, posy, box_w, box_h, shape, texts) {

    push();

    translate(posx, posy);
    let rows = shape[0];
    let cols = shape[1];
    let r_gap = box_h / (rows + 2);
    let c_gap = box_w / (cols + 2);

    strokeWeight(1);
    stroke(color(20, 20, 20));

    for (let i=0; i<rows; i++) {
        for (let j=0; j<cols; j++) {
            let x = (j+1) * c_gap;
            let y = (i+1) * r_gap;
            rect(x, y, c_gap, r_gap);
            textSize(20);
            textAlign(CENTER, CENTER);
            let idx = i * 3 + j;
            text(texts[idx].toFixed(1).toString(), x+0.5*c_gap, y+0.5*r_gap);
        }
    }
    pop();
}
